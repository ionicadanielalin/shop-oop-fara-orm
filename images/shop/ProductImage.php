<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/5/2019
 * Time: 8:45 AM
 */
class ProductImage
{
    public $id;
    public $url;
    public $product_id;

    public function __construct($id)
    {
        $data = dbSelectOne('product_images',['id'=>$id]);
        foreach ($data as $key => $value){
            $this->$key = $value;
        }
    }
}