<?php
include "config.php";


function dbSelect($table, $filters = [], $likeFilters = [], $offset = 0, $limit = null, $sortBy = null, $sortDiresction = 'ASC')
{
    global $dbConnection;
    $sql = "SELECT*FROM $table ";
    if ($filters != null || $likeFilters != null) {
        $sets = [];
        foreach ($filters as $column => $value) {
            $sets[] = "`" . mysqli_real_escape_string($dbConnection, $column) . "`='" . mysqli_real_escape_string($dbConnection, $value) . "'";
        }
        foreach ($likeFilters as $column => $value) {
            $sets[] = "`" . mysqli_real_escape_string($dbConnection, $column) . "`LIKE '%" . mysqli_real_escape_string($dbConnection, $value) . "'";
        }
        $sql .= ' WHERE ' . implode('AND', $sets);
    }

    if ($sortBy != null) {
        $sql .= ' ORDER BY ' . mysqli_real_escape_string($dbConnection, $sortBy) . '' . mysqli_real_escape_string($dbConnection, $sortDiresction);
    }

    if ($limit != null) {
        $sql .= ' LIMIT ' . intval($offset) . ',' . intval($limit);
    }
    $result = mysqli_query($dbConnection, $sql);

    if (!$result) {
        die("SQL error: " . mysqli_error($dbConnection) . "SQL:" . $sql);
    }
    return $result->fetch_all(MYSQLI_ASSOC);
}


function dbSelectOne($table, $filters = [], $likeFilters = [], $offset = 0, $limit = null, $sortBy = null, $sortDirection = 'ASC')
{
    $data = dbSelect($table, $filters, $likeFilters, 0, 1, $sortBy, $sortDirection);
    if (isset($data[0])) {
        return $data[0];
    } else {
        return null;
    }
}

function productView($product)
{
    ?>
    <div class="product-details">
        <div class="col-sm-5">
            <?php foreach ($product->getProductImages() as $image): ?>
                <div class="view-product">
                    <img src="images/shop/<?php echo $image->url; ?>" alt=""/>
                </div>
            <?php endforeach; ?>
        </div>
        <!--/product-information-->
        <div class="col-sm-7">
            <div class="product-information">
                <img src="images/product-details/new.jpg" class="newarrival" alt=""/>
                <h2><?php echo $product->name; ?></h2>
                <p>Web ID: <?php echo $product->id; ?></p>
                <p><b>Description:</b><?php echo $product->description; ?></p>
                <span>
									<span><?php echo $product->getFinalPrice(); ?> RON</span>
									<label>Quantity:</label>
									<input type="text" value="3"/>
									<a href="add_to_cart.php?product_id=<?php echo $product->id; ?>&quantity=1"
                                       class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
								</span>
                <p><b>Availability:</b> In Stock</p>
                <p><b>Condition:</b> New</p>
                <p><b>Brand:</b> E-SHOPPER</p>
                <a href=""><img src="images/product-details/share.png" class="share img-responsive" alt=""/></a>
            </div><!--/product-information-->
        </div>
    </div><!--/product-details-->
    <?php
}

function productView1($product)
{
    $products = dbSelect('product');
    foreach ($products as $product): ?>
        <div class="col-sm-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <img src="images/shop/product12.jpg" alt=""/>
                        <h2><?php echo $product['price']; ?></h2>
                        <p><?php echo $product['name']; ?></p>
                        <a href="add_to_cart.php?product_id=<?php echo $product->id; ?>&quantity=1"
                           class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add
                            to cart</a>
                    </div>
                </div>
                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                        <li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
                    </ul>
                </div>
            </div>
        </div>
    <?php endforeach;
}


function showSimilarProduct($product)
{
    ?>
    <div class="col-sm-4">
        <div class="product-image-wrapper">
            <div class="single-products">
                <div class="productinfo text-center">
                    <?php foreach ($product->getProductImages() as $image): ?>
                        <a href="product.php?id=<?php echo $product->id; ?>"><img
                                    src="images/shop/<?php echo $image->url; ?>"
                                    alt=""/></a>
                    <?php endforeach; ?>
                    <h2><?php echo $product->getFinalPrice(); ?> RON</h2>
                    <p><?php echo $product->name; ?></p>
                    <p>
                        <b>Description:</b><?php echo $product->description; ?>
                        ...</p>
                    <span>
                                               <a href="add_to_cart.php?product_id=<?php echo $product-> id;?>&quantity=1" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                </div>
            </div>
        </div>
    </div>
    <?php
}

function showSimilarImage($foto)
{
    ?>
    <div class="col-sm-4">
        <div class="product-image-wrapper">
            <div class="single-products">
                <div class="productinfo text-center">
                    <?php foreach ($foto->getProductImages() as $image): ?>
                        <img src="images/shop/<?php echo $image->url; ?>" alt=""/></a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}

function dbInsert($table, $data)
{
    global $dbConnection;
    $columns = [];
    $values = [];
    //$images = $_FILES['image'];

    foreach ($data as $column => $value) {
        $columns[] = "`" . mysqli_real_escape_string($dbConnection, $column) . "`";
        $values[] = "'" . mysqli_real_escape_string($dbConnection, $value) . "'";
    }
    $sqlcolumns = implode(',', $columns);
    $sqlvalues = implode(',', $values);
    $sql = "INSERT INTO $table($sqlcolumns) VALUES ($sqlvalues)";
    $result = mysqli_query($dbConnection, $sql);

    if ($error = mysqli_error($dbConnection)){
        die("SQL error: " . mysqli_error($dbConnection) . "SQL:" . $sql);
    }
    return mysqli_insert_id($dbConnection);
}


function dbDelete($table, $id)
{
    global $dbConnection;
    $sql = "DELETE FROM $table WHERE id=" . intval($id);
    $dbConnection->query($sql);
    return mysqli_affected_rows($dbConnection) > 0;
}


function dbUpdate($table, $id, $data)
{
    global $dbConnection;
    $sets = [];
    foreach ($data as $column => $value) {
        $sets[] = "`" . mysqli_real_escape_string($dbConnection, $column) . "`='" . mysqli_real_escape_string($dbConnection, $value) . "'";
    }
    $sqlSets = implode(',', $sets);

    $result = mysqli_query($dbConnection, "UPDATE $table SET $sqlSets  WHERE id=" . intval($id));

    return mysqli_affected_rows($dbConnection) > 0;

}

function showCategory($categoryItem)
{
    ?>
    <div class="panel-body">
        <ul>
            <li>
                <a href="category.php?category=<?php echo $categoryItem['id']; ?>"><?php echo $categoryItem['name']; ?></a>
            </li>
        </ul>
    </div>
    <?php
}

function showCategoryPage($products)
{
    foreach ($products as $product) {
        ?>
        <div class="col-sm-9 padding-right">
            <!--product-details-->
            <div class="product-details">
                <div class="col-sm-5">
                    <?php
                    //$product= new Product($product);
                    foreach ($product->getProductImages() as $image): ?>
                        <div class="view-product">
                            <a href="product.php?id=<?php echo $product->id; ?>"> <img
                                        src="images/shop/<?php echo $image->url; ?>"></a>
                        </div>
                    <?php endforeach; ?>
                </div>

                <!--/product-information-->
                <div class="col-sm-7">
                    <div class="product-information">
                        <img src="images/product-details/new.jpg" class="newarrival" alt=""/>
                        <h2><?php echo $product->name; ?></h2>
                        <p>Web ID: <?php echo $product->id; ?></p>
                        <p><b>Description:</b><?php echo $product->description; ?></p>
                        <span>
									<span><?php echo $product->getFinalPrice(); ?> RON</span>
									<label>Quantity:</label>
									<input type="text" value="3"/>
									<a href="add_to_cart.php?product_id=<?php echo $product->id; ?>&quantity=1"
                                       class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
								</span>
                        <p><b>Availability:</b> In Stock</p>
                        <p><b>Condition:</b> New</p>
                        <p><b>Brand:</b> E-SHOPPER</p>
                        <a href=""><img src="images/product-details/share.png" class="share img-responsive" alt=""/></a>
                    </div><!--/product-information-->
                </div>
            </div><!--/product-details-->
        </div>
        <?php
    }
}

function showComment($comment)
{
    ?>
    <div>
        <ul>
            <li></i><?php echo $comment['nickname']; ?></a></li>
            <li></i><?php echo $comment['date']; ?></a></li>
        </ul>
        <p><?php echo $comment['content']; ?></p>
    </div>
    <?php
}

