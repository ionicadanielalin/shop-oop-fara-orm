<?php
include "includes/parts/header.php";
include "includes/functions.php";

session_start();

if (isset($_SESSION['cart_id'])) {
    $cart = new Cart($_SESSION['cart_id']);
} else {
    $cart = new Cart();
    $cart->save();
    $_SESSION['cart_id'] = $cart->getId();
}
?>

<body>

<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li class="active">Shopping Cart</li>
            </ol>
        </div>
        <!-- TABEL AFISARE PRODUSE DIN COS-->

        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <td class="image">Item</td>
                    <td class="description"></td>
                    <td class="price">Price</td>
                    <td class="quantity">Quantity</td>
                    <td class="total">Total</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($cart->getCartItems() as $cartItem): ?>

                    <tr>
                        <td class="cart_product">
                            <a href=""><img src="images/cart/one.png" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4 style="color: black"><?php echo $cartItem->getProduct()->name; ?></h4>
                        </td>
                        <td class="cart_price">
                            <p><?php echo $cartItem->getProduct()->getFinalPrice(); ?></p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a class="cart_quantity_up"
                                   href="add_to_cart.php?product_id=<?php echo $cartItem->product_id; ?>&quantity=1">
                                    + </a>
                                <input class="cart_quantity_input" type="text" name="quantity" value="<?php echo $cartItem->quantity; ?>"
                                       autocomplete="off" size="2">
                                <a class="cart_quantity_down"
                                   href="add_to_cart.php?product_id=<?php echo $cartItem->getProduct()->id; ?>&quantity=-1">
                                    - </a>
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price"><?php echo $cartItem->getTotal(); ?></p>
                        </td>
                        <td class="cart_delete">
                            <!-- "add_to_cart.php?product_id=<?php// echo $cartItem->getProduct()->id; ?>&quantity=0" -->
                            <a class="cart_quantity_delete" href="deleteCartItem.php?product_id=<?php echo $cartItem->getProduct()->id; ?>"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">
        <div class="heading">
            <h3>What would you like to do next?</h3>
            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your
                delivery cost.</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="chose_area">
                    <ul class="user_option">
                        <li>
                            <input type="checkbox">
                            <label>Use Coupon Code</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Use Gift Voucher</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Estimate Shipping & Taxes</label>
                        </li>
                    </ul>
                    <ul class="user_info">
                        <li class="single_field">
                            <label>Country:</label>
                            <select>
                                <option>United States</option>
                                <option>Bangladesh</option>
                                <option>UK</option>
                                <option>India</option>
                                <option>Pakistan</option>
                                <option>Ucrane</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>

                        </li>
                        <li class="single_field">
                            <label>Region / State:</label>
                            <select>
                                <option>Select</option>
                                <option>Dhaka</option>
                                <option>London</option>
                                <option>Dillih</option>
                                <option>Lahore</option>
                                <option>Alaska</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>

                        </li>
                        <li class="single_field zip-field">
                            <label>Zip Code:</label>
                            <input type="text">
                        </li>
                    </ul>
                    <a class="btn btn-default update" href="">Get Quotes</a>
                    <a class="btn btn-default check_out" href="">Continue</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="total_area">
                    <ul>
                        <li>Cart Sub Total <span> $ <?php echo $cart->getTotal(); ?></span></li>
                        <li>Eco Tax <span>$ 0</span></li>
                        <li>Shipping Cost <span>Free</span></li>
                        <li>Total <span> $ <?php echo $cart->getTotal(); ?></span></li>
                    </ul>
                    <a class="btn btn-default update" href="">Update</a>
                    <a class="btn btn-default check_out" href="cart_to_order.php">Check Out</a>
                </div>
            </div>
        </div>
    </div>
</section><!--/#do_action-->

<?php include "includes/parts/footer.php" ?>
</footer><!--/Footer-->


<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/main.js"></script>
</body>
</html>