<?php

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 8/22/2019
 * Time: 8:11 PM
 */
class Cart extends BaseEntity
{
    const RAMBURS_PAYMENT = 'ramburs';

    public $user_id;

    public $address;

    public $phone;

    public $email;

    public $payment_method = self::RAMBURS_PAYMENT;

    public function getUser()
    {
        return new User($this->user_id);
    }
    public function getCustomer()
    {
        return new Customer($this->user_id);
    }

    public function getCartItems()
    {
        $data = dbSelect('cart_item', ['cart_id' => $this->getId()]);

        $result = [];
        foreach ($data as $productData) {
            $result[] = new CartItem($productData['id']);
        }

        return $result;
    }

    public function getTotal()
    {
        $total = 0;

        foreach ($this->getCartItems() as $cartItem) {
            $total += $cartItem->getTotal();
        }

        return $total;
    }

    public function add($product_id, $quantity)
    {
        foreach ($this->getCartItems() as $cartItem) {
            if ($cartItem->product_id == $product_id) {
                $cartItem->quantity += $quantity;
                if ($cartItem->quantity <= 0) {
                    $cartItem->delete();
                } else {
                    $cartItem->save();
                }
                return;
            }
        }
        $cartItem = new CartItem();
        $cartItem->cart_id = $this->getId();
        $cartItem->product_id = $product_id;
        $cartItem->quantity = $quantity;
        $cartItem->save();
    }

    public function update($product_id, $quantity)
    {
        foreach ($this->getCartItems() as $cartItem) {
            if ($cartItem->product_id == $product_id) {
                $cartItem->quantity = $quantity;
                if ($quantity <= 0) {
                    $cartItem->delete();
                } else {
                    $cartItem->save();
                }
            }
        }
    }

    public function emptyCart()
    {
        foreach ($this->getCartItems() as $cartItem) {
            $cartItem->delete();
        }
    }
//public function generateOrder ($cart_id)
//{
//    foreach ($this->getCartItems() as $cartItem) {
//
//        $order = new Order();
//
//        $order->cart_id = $this->getId();
//        $order->user_id = $this->user_id;
//        $order->product_id = $cartItem->product_id;
//        $order->quantity = $cartItem->quantity;
//        $order->product_name = $cartItem->getProduct()->name;
//        $order->product_final_price = $cartItem->getTotal();
//        $order->total_price = $this->getTotal();
//        $order->user_first_name = $this->getCustomer()->firstname;
//        $order->user_last_name = $this->getCustomer()->lastname;
//        $order->state = $this->getCustomer()->state;
//        $order->city = $this->getCustomer()->city;
//        $order->street = $this->getCustomer()->street;
//        $order->nr = $this->getCustomer()->nr;
//        $order->phone = $this->getCustomer()->phone;
//        $order->payment_method = $this->payment_method;
//        $order->date = date("YMD");
//        $order->status = 'xxx';
//
//        $order->save();
//    }
//
//
//}

}
