<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/12/2019
 * Time: 11:29 AM
 */
class Order extends BaseEntity
{
    const RAMBURS_PAYMENT = 'ramburs';

    public $cart_id;

    public $user_id;

    public $user_first_name;

    public $user_last_name;

    public $state;

    public $city;

    public $street;

    public $nr;

    public $phone;

    public $payment_method = self::RAMBURS_PAYMENT;

    public $date;

    public $status;

    public function getTable()
    {
        return 'orders';
    }


    public function getCustomer()
    {
        return new Customer($this->user_id);
    }
    public function getCart ()
    {
        return new Cart($this->cart_id);
    }

    public function getOrderItems()
    {
        $data = dbSelect('order_item', ['order_id' => $this->getId()]);

        $result = [];
        foreach ($data as $productData) {
            $result[] = new OrderItem($productData['id']);
        }

        return $result;
    }

    public function getOrderTotal()
    {
        $total = 0;

        foreach ($this->getOrderItems() as $orderItem) {
            $total += $orderItem->getTotal();
        }

        return $total;
    }

    public function addOrder($product_id, $quantity)
    {
        foreach ($this->getOrderItems() as $orderItem) {
            $orderItem->save();

            $orderItem = new OrderItem;
            $orderItem->order_id = $this->getId();
            $orderItem->product_id = $product_id;
            $orderItem->quantity = $quantity;
            $orderItem->save();
        }
    }

    public function updateOrder($product_id, $quantity)
    {
        foreach ($this->getOrderItems() as $orderItem) {
            if ($orderItem->product_id == $product_id) {
                $orderItem->quantity = $quantity;
                if ($quantity <= 0) {
                    $orderItem->delete();
                } else {
                    $orderItem->save();
                }
            }
        }
    }

    public function emptyOrder()
    {
        foreach ($this->getOrderItems() as $orderItem) {
            $orderItem->delete();
        }
    }

}