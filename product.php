<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Product Details | E-Shopper</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="includes/css/bootstrap.min.css" rel="stylesheet">
    <link href="includes/css/font-awesome.min.css" rel="stylesheet">
    <link href="includes/css/prettyPhoto.css" rel="stylesheet">
    <link href="includes/css/price-range.css" rel="stylesheet">
    <link href="includes/css/animate.css" rel="stylesheet">
    <link href="includes/css/main.css" rel="stylesheet">
    <link href="includes/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

    <![endif]-->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css"/>
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<script>
    $(document).ready(function () {
        var date_input = $('input[name="date"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var options = {
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>
<?php
session_start();
include "includes/parts/header.php";
include "includes/functions.php";
if (isset($_GET['id'])) {
    $_SESSION['id'] = $_GET['id'];
}
$product = new Product($_GET['id']);
?>

<section>
    <div class="container">
        <div class="row">
            <?php include "includes/parts/sidebar.php" ?>


            <div class="col-sm-9 padding-right">
                <!--product-details-->
                <?php productView($product) ?>
                <!-- More images of product -->

                <div class="recommended_items">
                    <h2 class="title text-center">Imagini ale produsului</h2>
                    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <?php
                                $productData = dbSelect('product_images', ['id' => $_GET['id']], [], 0, 0, null, ' DESC');
                                foreach ($productData as $productItem) {
                                    $product = new Product($productItem['id']);
                                    showSimilarImage($product);
                                }
                                ?>
                            </div>
                        </div>
                        <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>


                <!--recommended_items-->
                <div class="recommended_items">
                    <h2 class="title text-center">din aceeasi categorie</h2>
                    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <?php
                                $productData = dbSelect('product', ['category_id' => $product->category_id], [], 0, 3, 'RAND()', ' DESC');
                                foreach ($productData as $productItem) {
                                    $sproduct = new Product($productItem['id']);
                                    showSimilarProduct($sproduct);
                                }
                                ?>
                            </div>
                        </div>
                        <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
                <!--/recommended_items-->

                <!--Comments-->
                <!--category-tab-->
                <div class="category-tab shop-details-tab">

                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#reviews" data-toggle="tab">Write a review</a></li>
                            <li><a href="#details" data-toggle="tab">Reviews</a></li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane fade" id="details">
                            <div class="tab-pane fade active in" id="reviews">

                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <?php foreach ($product->getComments() as $comment): ?>
                                            <ul>
                                                <li><a href=""><i class="fa fa-user"></i><?php echo $comment->nickname; ?></a></li>
                                                <li><a href=""><i class="fa fa-calendar-o"></i><?php echo $comment->date; ?></a></li>
                                                <p><?php echo $comment->content; ?></p>
                                            </ul>
                                        <?php endforeach;?>
                                    </div>

                                    <div class="col-sm-12">

                                        <p><b>Write Your Review</b></p>

                                        <form action="add-comment.php?id=<?php echo $_GET['id']; ?>" method="post">
                                            <div class="col-sm-12">
                                                <div class="input-group-prepend">
                                        <span>
											<input name="nickname" type="text" placeholder="Your Name"/>
											<input name="email" type="email" placeholder="Email Address"/>
										</span>
                                                </div>
                                                <!-- Date picker -->
                                                <div class="bootstrap-iso">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                                <div class="form-group"> <!-- Date input -->
                                                                    <label class="control-label" for="date">Date</label>
                                                                    <input class="form-control" id="date" name="date"
                                                                           placeholder="yyyy/mm/dd" type="text"/>
                                                                </div>
                                                                <!-- Form code ends -->

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Date picker -->

                                                <div class="col-sm-12">
                                                    <div class="input-group-prepend">
                                                        <textarea name="content"></textarea>

                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-default pull-right">
                                                    Submit
                                                </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!--/category-tab-->
                    </div>
                </div>
            </div>
        </div>
</section>


<?php
//$comments = dbSelect('comments', ['product_id' => $_GET['id']], [], 0, null, null, ' DESC');

//foreach ($comments as $comment) {
//var_dump($comment);
//showComment($comment);
//}
?>


<!--/Footer-->
<?php include "includes/parts/footer.php"; ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

<script src="js/jquery.js"></script>
<script src="js/price-range.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/main.js"></script>
</body>
</html>


<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script>
    $(document).ready(function () {
        var date_input = $('input[name="date"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var options = {
            format: 'yyyy/mm/dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>