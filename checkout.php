<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include "includes/functions.php";
    session_start();
    $order = new Order($_GET['order_id']);
    ?>

</head><!--/head-->

<?php include "includes/parts/header.php"; ?>

<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Check out</li>
            </ol>
        </div><!--/breadcrums-->

        <div class="step-one">
            <h2 class="heading">Step1</h2>
        </div>
        <div class="checkout-options">
            <h3>New User</h3>
            <p>Checkout options</p>
            <ul class="nav">
                <li>
                    <label><input type="checkbox"> Register Account</label>
                </li>
                <li>
                    <label><input type="checkbox"> Guest Checkout</label>
                </li>
                <li>
                    <a href="cancel_order.php?order_id=<?php echo $order->getId() ?>"><i class="fa fa-times"></i>Cancel</a>
                </li>
            </ul>
        </div><!--/checkout-options-->

        <div class="register-req">
            <p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
        </div><!--/register-req-->

        <div class="shopper-informations">
            <div class="row">
                <div class="col-sm-3">
                    <div class="shopper-info">
                        <p>Shopper Information</p>
                        <form>
                            <input type="text" placeholder="Display Name">
                            <input type="text" placeholder="User Name">
                            <input type="password" placeholder="Password">
                            <input type="password" placeholder="Confirm password">
                        </form>
                        <a class="btn btn-primary" href="">log in</a>
                    </div>
                </div>
                <
                <div class="col-sm-5 clearfix">
                    <div class="bill-to">
                        <p>Bill To</p>

                        <div class="form-one">
                            <form action="add-customer.php" method="POST">
                                <input type="text" placeholder="Company Name">
                                <input name="email" type="text" class="form-control" placeholder="Email*">
                                <input name="firstname" type="text" class="form-control" placeholder="First Name *">
                                <input name="lastname" type="text" class="form-control" placeholder="Last Name *">
                                <input name="street" type="text" class="form-control" placeholder="Street *">
                                <input name="number" type="text" class="form-control" placeholder="Street number">
                                <input name="city" type="text" class="form-control" placeholder="city">
                                <button type="submit" class="btn btn-primary mb-2" href="add-customer.php?order_id="<?php echo $order->getId() ?>>Salveaza datele
                                </button>
                            </form>
                        </div>
                        <div class="form-two">
                            <form action="add-customer.php" method="POST">
                            <input type="text" placeholder="Zip / Postal Code *">
                            <select name="counntry">
                                <option>-- Country --</option>
                                <option>United States</option>
                                <option>Bangladesh</option>
                                <option>UK</option>
                                <option>India</option>
                                <option>Pakistan</option>
                                <option>Ucrane</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                            <select name="state">
                                <option>-- State / Province / Region --</option>
                                <option>United States</option>
                                <option>Bangladesh</option>
                                <option>UK</option>
                                <option>India</option>
                                <option>Pakistan</option>
                                <option>Ucrane</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>
                            <input name="phone" type="text" class="form-control" placeholder="Phone *">
                            <input name="password" type="password" class="form-control" placeholder="Password">
                            <input type="password" class="form-control" placeholder="Confirm password">

                            <button type="submit" class="btn btn-primary mb-2" href="add-customer.php">Salveaza datele
                            </button>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="order-message">
                        <p>Shipping Order</p>
                        <textarea name="message" placeholder="Notes about your order, Special Notes for Delivery"
                                  rows="16"></textarea>
                        <label><input type="checkbox"> Shipping to bill address</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="review-payment">
            <h2>Review & Payment</h2>
        </div>

        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <td class="image">Item</td>
                    <td class="description"></td>
                    <td class="price">Price</td>
                    <td class="quantity">Quantity</td>
                    <td class="total">Total</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>

                <?php
                //var_dump($order);
                foreach ($order->getOrderItems() as $orderItem): ?>

                    <tr>
                        <td class="cart_product">
                            <a href=""><img src="images/cart/one.png" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4 style="color: black"><?php echo $orderItem->product_name; ?></h4>
                        </td>
                        <td class="cart_price">
                            <p><?php echo $orderItem->price; ?></p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">

                                <input class="cart_quantity_input" type="text" name="quantity"
                                       value="<?php echo $orderItem->quantity; ?>"
                                       autocomplete="off" size="2">
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price"><?php echo $orderItem->getTotal(); ?></p>
                        </td>

                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right">
                        <a class="btn btn-default check_out" style="" href="thankYou.php">Order Now</a>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
            <span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
            <span>
						<label><input type="checkbox"> Paypal</label>
					</span>
        </div>
    </div>
</section> <!--/#cart_items-->


<?php include "includes/parts/footer.php" ?>


<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/main.js"></script>
</body>
</html>